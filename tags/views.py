# from django.shortcuts import render
from django.views.generic.list import ListView
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

try:
    from tags.models import Tag
except Exception:
    Tag = None


# Create your views here.
class TagViewList(ListView):
    model = Tag
    template_name = "tags/list.html"
    paginate_by = 2


class TagCreateView(CreateView):
    model = Tag
    template_name = "tags/new.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("tags_list")


class TagUpdateView(UpdateView):
    model = Tag
    template_name = "tags/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("tags/list")


class TagDetailView(DetailView):
    model = Tag
    template_name = "tags/detail.html"


class TagDeleteView(DeleteView):
    model = Tag
    template_name = "tags/delete.html"
    success_url = reverse_lazy("tags_list")
